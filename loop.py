#!/usr/bin/env python2
#  -*- coding: utf-8 -*-

# from rfid_sl500_p2 import SL500_RFID_Reader
from ControllerSystem import ControllerSystem
from errors import *
import sys
from time import sleep
import log_system

log_system.init_logger('app')
logger = log_system.get_logger('app')


def main():
    while True:
        logger.info('Инициализируется система контроллеров')
        try:
            system = ControllerSystem()
        except:
            logger.warn('Неудачная попытка инициализации')
            sleep(1)
            continue
        logger.info('Система контроллеров проинициализирована')
        try:
            while True:
                logger.info('Начинается сбор и обработка данных')
                system.process_data()
                sleep(1)
        except KeyboardInterrupt:
            logger.info('Пользователь прервал программу')
            sleep(1)
            sys.exit(1)
        except Exception as e:
            logger.error(
                'Вызвалось исключение, приведшее к перезапуску '
                'программы: {} - {}'.format(type(e).__name__, e))
            continue


if __name__ == '__main__':
    main()





