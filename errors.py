#!/usr/bin/env python
#  -*- coding: utf-8 -*-


class ReaderIOError(Exception):
    pass


class DBConnectionRefusedError(Exception):
    pass


class TimeoutRaisedError(Exception):
    pass


class ConnectionHelperError(Exception):
    pass


class ReaderHelperError(Exception):
    pass


class DataTypeError(Exception):
    pass
