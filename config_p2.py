#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import ConfigParser


def read_config_section(section, filename='cs_system.ini'):
    parser = ConfigParser.ConfigParser()
    parser.read(filename)
    # print(parser._sections)

    if parser.has_section(section):
        d_section = parser._sections[section]
        del d_section['__name__']
        return d_section
    else:
        raise AttributeError('{0} не найдено в файле {1}'.format(section, filename))


def read_config_option(section, option, param_type='str', filename='cs_system.ini'):
    parser = ConfigParser.ConfigParser()
    parser.read(filename)
    if parser.has_section(section) and option in parser.options(section):
        callback = get_func_with_type(parser, param_type)
        value = callback(section, option)
        if type(value) == str:
            value = value.encode('utf-8')
        return value
    else:
        raise AttributeError('{0} не найдено в разделе {1} '.format(option, section))


def get_func_with_type(parser, type):
    if type == 'str' or type == 'string':
        return parser.get
    elif type == 'int' or type == 'integer':
        return parser.getint
    elif type == 'float':
        return parser.getfloat
    elif type == 'bool' or type == 'boolean':
        return parser.getboolean
    else:
        raise ValueError('Не существует функции для заданного типа {}'.format(type))