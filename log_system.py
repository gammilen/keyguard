#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import logging
from functools import wraps
import datetime
from logging.handlers import TimedRotatingFileHandler
import re


def init_logger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    logname = "log/keyguard.log"
    handler = TimedRotatingFileHandler(logname, when="midnight", interval=1, backupCount=30)
    handler.suffix = "%d%m%Y"
    handler.extMatch = re.compile(r"^\d{8}$")

    # handler = logging.FileHandler('log/{}app.log'.format(datetime.date.today().strftime('%d%m%Y')))
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s [%(levelname)s]: %(name)s - %(message)s')
    handler.setFormatter(formatter)

    handler2 = logging.StreamHandler()
    handler2.setLevel(logging.DEBUG)
    formatter2 = logging.Formatter('[%(levelname)s]: %(message)s')
    handler2.setFormatter(formatter2)

    logger.addHandler(handler)
    logger.addHandler(handler2)


def get_logger(name):
    return logging.getLogger(name)


def debug_exceptions_decorator(logger):
    def wrap(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            # logger = get_logger(log_name)
            try:
                response = function(*args, **kwargs)
            except Exception as error:
                logger.debug(
                    "Function '{}' raised {} with error '{}'".format(
                        function.__name__, error.__class__.__name__,
                        str(error)))
                raise error
            return response
        return wrapper
    return wrap


def debug_func_call_decorator(logger, input=True, output=True, exc=True):
    def wrap(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            # logger = get_logger(log_name)
            if input is True:
                logger.debug(
                    "Calling function '{}' with args={} kwargs={}".format(
                        function.__name__, args, kwargs))
            else:
                logger.debug(
                    "Calling function '{}'".format(function.__name__))
            try:
                response = function(*args, **kwargs)
            except Exception as error:
                if exc is True:
                    logger.debug(
                        "Function '{}' raised {} with error '{}'".format(
                            function.__name__, error.__class__.__name__,
                            str(error)))
                raise error
            else:
                if output is True:
                    logger.debug(
                        "Function '{}' end and returned {}".format(
                            function.__name__, response))
                else:
                    logger.debug(
                        "Function '{}' end".format(function.__name__))


            return response
        return wrapper
    return wrap
