#!/usr/bin/env python
#  -*- coding: utf-8 -*-

# from rfid_sl500_p2 import SL500_RFID_Reader
from errors import *
import signal
import config_p2 as config
from functools import wraps
# from connectors import RemoteDBConnector #, LocalDBConnector
from operations import OperationFactory
from helpers import HelperManager
import log_system
from time import sleep


class ControllerSystem(object):
    logger = log_system.get_logger('app.cs')

    def __init__(self):
        self.helper = HelperManager()
        self.helper.reader.set_indication('init')
        self.indicate()

    def block(self):
        self.logger.info('Поставлен блок на считывание')
        self.helper.reader.set_indication('block')
        self.indicate()
        self.helper.connection.reconnect()
        self.helper.reader.set_indication('default')
        self.indicate()
        self.helper.reader.set_indication('init')

    def refresh_readers(self):
        self.logger.info('Выполняется попытка восстановления соединения со считывателями')
        self.helper.reader.reconnect()
        self.helper.reader.set_indication('init')

    @log_system.debug_func_call_decorator(logger, False, False, False)
    def process_data(self):
        try:
            self.wait_interaction()
            self.init_data_type()
        except TimeoutRaisedError:
            self.logger.info('Время ожидания данных окончено')
            timeout = True
        except ReaderIOError as e:
            self.logger.error(
                'Во время считывания возникло исключение: '
                '{} - {}'.format(type(e).__name__, e))
            self.helper.session.end()
            self.refresh_readers()
            self.indicate()
            return None
        except:
            self.helper.reader.set_indication('error')
            self.indicate()
            raise
        else:
            timeout = False

        operation = self.create_operation(timeout)
        self.logger.info('Начинается обработка операции')
        try:
            operation.tag_log()
            operation.execute()
        except DBConnectionRefusedError as e:
            self.logger.error(
                'Ошибка соединения с базой данных: '
                '{} - {}'.format(type(e).__name__, e))
            self.helper.session.end()
            self.block()
        except:
            self.helper.reader.set_indication('error')
            raise
        else:
            self.logger.info('Операция была выполнена успешно')
        finally:
            operation.close()
            self.indicate()

    @log_system.debug_func_call_decorator(logger, False, False, False)
    def wait_interaction(self):
        self.logger.info('Ожидание взаимодействия')
        if self.helper.session.status:
            sec = config.read_config_option(
                    'system', 'timeout_seconds', 'int')
            self.logger.info('Запущен таймаут в {} сек.'.format(sec))
            timeout(sec)(self.helper.reader.wait_data)()
            self.logger.info('Данные получены до окончания таймаута')
        else:
            test_connection(self.helper.connection)(self.helper.reader.wait_data)()

    def init_data_type(self):
        self.logger.info('Определяется тип данных приложенного объекта')
        try:
            reader = self.helper.reader
            reader.try_to_recognize()
            if not reader.data_type:
                reader.data_type = 'card'
            self.logger.info('Распознанный тип данных: {}'.format(reader.data_type))
        except DataTypeError:
            self.logger.info('Не удалось определить тип объекта')

    def indicate(self):
        self.logger.info('Происходит индикация на считывателях')
        self.helper.reader.indicate()

    def create_operation(self, timeout):
        self.logger.info('Определяется тип операции по имеющимся данным')
        operation = OperationFactory.create_operation(
            self.helper.reader.data_type, self.helper.reader.side,
            self.helper.session.status, timeout)
        operation.helper = self.helper
        return operation

# - - - - - - - - - - -


def timeout(timeout_sec):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutRaisedError(
                'Прошло время ожидания взаимодействия')

        #@log_system.debug_func_call_decorator(module_logger, True)
        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            # signal.setitimer(signal.ITIMER_REAL, sec)
            signal.alarm(timeout_sec)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result
        return wraps(func)(wrapper)
    return decorator


def test_connection(connection):
    def decorator(func):
        def wrapper(*args, **kwargs):
            connection.test.set()
            result = func(*args, **kwargs)
            connection.test.clear()
            return result
        return wraps(func)(wrapper)
    return decorator
