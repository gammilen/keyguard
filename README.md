### Для полноценной работы необходимо подключить сервисы

Измените в файле *service/keyguard.service* поля User и Group на текущие для системы, в файле *service/chromium.desktop* - link - ссылку, по которой доступен просмотр состояния ключей

Скопируйте файл *service/keyguard.service* в папку */etc/systemd/system*, после чего выполните команду systemctl enable keyguard для автоматического старта

Скопируйте файл *service/chromium.desktop* в <userFolder>/.config/autostart, где <userFolder> - папка пользователя, от которого запускается система

### Настройка VNC

Для того, чтобы настроить удаленный доступ, необходимо установить vino
`sudo apt-get install vino`
`sudo apt-get install dconf-editor`

Открываем dconf-editor, заходим в org → gnome → desktop → remote access 

Устанавливаем порт: 5900, метод: vnc, отмечаем галочками только enabled и notify-on-connect, также ставим пароль, который можно получить командой `echo -n mysuperpassword | base64`

Необходимо установить DISPLAY: `set DISPLAY=:0`

В настройках Session and Startup заносим команду `/usr/lib/vino/vino-server`