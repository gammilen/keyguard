#!/usr/bin/env python
#  -*- coding: utf-8 -*-
from errors import *
import config_p2 as config
import mysql.connector
# from mysql.connector import errorcode
from rfid_sl500_p2 import SL500_RFID_Reader
import json
from time import sleep
import log_system
import datetime
import multiprocessing.dummy as multiprocessing
from multiprocessing.pool import ThreadPool
from multiprocessing import Process
import threading

class HelperManager(object):
    logger = log_system.get_logger('app.helper')

    @log_system.debug_func_call_decorator(logger, False, False, True)
    def __init__(self):
        self.reader = ReaderHelper()
        self.connection = ConnectionHelper()
        self.session = Session()


class Session(object):
    logger = log_system.get_logger('app.session')

    def __init__(self):
        self.status = False
        self.uid = None
        self.sspn = None
        self.supn = None

    @log_system.debug_func_call_decorator(logger, False, False, False)
    def start_session(self):
        self.logger.info('Сеанс начинается')
        self.status = True

    @log_system.debug_func_call_decorator(logger, False, False, False)
    def end(self):
        self.logger.info('Сеанс завершается')
        self.status = False
        self.uid = None
        self.sspn = None
        self.supn = None


class ConnectionHelper(object):
    backup_file = 'connection_backup.json'
    logger = log_system.get_logger('app.connection')

    def __init__(self):
        self.remote = self.connect(
            **config.read_config_section('mysql.remote'))
        self.sigur = self.connect(
            **config.read_config_section('mysql.sigur'))
        self._request_queue = None
        self.test = threading.Event()
        # self.reconnection = threading.Event()
        self.testing_thread = threading.Thread(target=self.test_connection)
        self.testing_thread.daemon = True
        self.testing_thread.start()

        # try:
            # self.local = self.connect(**config.read_config_section('mysql.local'))
        # except AttributeError:
            # raise ConnectionHelperError('Невереные параметры локального соединения, проверьте конфигурацию')
        # except mysql.connector.Error as e:
            # raise ConnectionHelperError('Ошибка подключения к локальной базе: {}'.format(e))			

	#здесь решается, какой коннектор используется
    @property
    # @log_system.debug_func_call_decorator(logger, False, True, False)
    def connector(self):
        return self.remote

    @property
    # @log_system.debug_func_call_decorator(logger, False, True, False)
    def sigur_connector(self):
        return self.sigur
	
    @log_system.debug_func_call_decorator(logger, False, True, False)
    def connect(self, host, port, database, user, password):
        try:
            connector = mysql.connector.connect(
                charset='utf8mb4', collation='utf8mb4_general_ci', use_unicode=True,
                host=host, port=port, database=database,
                user=user, password=password)
        except mysql.connector.Error as e:
            raise ConnectionHelperError(
                'Неудачная попытка подключения к базе данных: {}'.format(e))
        else:
            return connector

    @log_system.debug_func_call_decorator(logger, False, False, False)
    def reconnect(self):
        while True:
            try:
                self._reconnect_base()
                self.logger.info('Выполняются все невыполненные запросы')
                for i in range(len(self.request_queue)):
                    self.execute_request_from_queue()
            except (mysql.connector.InterfaceError, DBConnectionRefusedError) as e:
                self.logger.debug('Неудачная попытка соединения с бд: {}'.format(e))
            else:
                break
            sleep(600)

    def test_connection(self):
        self.logger.debug('Начинаем проверку соединения')
        while True:
            while self.test.is_set():
                try:
                    self._reconnect_base()
                except mysql.connector.InterfaceError as e:
                    self.logger.debug('Неудачная попытка соединения с бд: {}'.format(e))
                except:
                    pass
                # else:
                #     self.logger.debug('Установили соединение в тестовом потоке')
                #     self.test.clear()
                #     continue
                sleep(100)
            sleep(60)

    def _reconnect_base(self):
        for conn in [self.connector, self.sigur]:
            if not conn.is_connected():
                self.logger.info('Выполняется попытка восстановления соединения с бд')
                conn.reconnect(10, 5)
                self.logger.info('Произведено восстановление соединения')

    @property
    def request_queue(self):
        if not self._request_queue:
            try:
                self._request_queue = json.load(open(self.backup_file))
            except ValueError:
                self._request_queue = []
        return self._request_queue

    @log_system.debug_func_call_decorator(logger, False, False, True)
    def add_request_into_queue(self, query, parameters):
        data = {
            'query': query,
            'parameters': parameters
        }
        self.request_queue.append(data)
        open(self.backup_file, 'w').write(json.dumps(self.request_queue))

    @log_system.debug_func_call_decorator(logger, False, False, True)
    def execute_request_from_queue(self):
        try:
            request = self.request_queue.pop(0)
        except IndexError:
            self.logger.debug('Список запросов пуст')
        else:
            # for key, value in request['parameters'].items():
            #     if isinstance(value, unicode):
            #         request['parameters'][key] = value.encode('utf8')
            # self.write_to_db(request['query'].encode('utf8'), request['parameters'])
            self.write_to_db(
                self.connector, request['query'], request['parameters'])
            open(self.backup_file, 'w').write(
                json.dumps(self.request_queue))

    # def check_connection_error(self, errno):
    # 	e = errorcode
    # 	if errno in [
    # 		e.CR_CONN_HOST_ERROR,
    # 		e.CR_SERVER_GONE_ERROR,
    # 		e.CR_SERVER_LOST,
    # 		e.CR_SERVER_LOST_EXTENDED,
    # 	]:
    # 		return True
		
    @log_system.debug_func_call_decorator(logger, True, False, False)
    def get_user_info(self, card):
        self.logger.info('Получаются данные о пользователе из базы')
        query = """ SELECT * 
                    FROM `sigur_users`
                    WHERE tag_value = %(tag_value)s """
        return self.read_from_db(self.connector, query, {'tag_value': card}, False)

    @log_system.debug_func_call_decorator(logger, True, False, False)
    def get_user_pn(self, code):
        self.logger.info('Получаются данные о номере пользователя из базы')
        query = """ SELECT  TABID 
                    FROM `personal`
                    WHERE CODEKEY = %(codekey)s """
        result = self.read_from_db(self.sigur, query, {'codekey': code}, False)
        if not result:
            query = """ SELECT p.TABID
                        FROM personal_keys AS k
                        JOIN personal AS p
                        ON p.ID = k.EMP_ID
                        WHERE k.CODEKEY = %(codekey)s """
            result = self.read_from_db(self.sigur, query, {'codekey': code}, False)
        return result

    @log_system.debug_func_call_decorator(logger, True, False, False)
    def add_security_number_for_seance(self, seance_id, sspn):
        self.logger.info('Заполняется поле номера охранника в запросе доступа')
        query = """ UPDATE `keyguard_accesslog`
                    SET sigur_security_pn = %(sigur_security_pn)s
                    WHERE seance_uid = %(seance_id)s """
        self.write_to_db(self.connector, query, {
            'seance_id': seance_id, 'sigur_security_pn': sspn})

    @log_system.debug_func_call_decorator(logger, True, False, False)
    def get_key_info(self, tag_value):
        self.logger.info('Получаются данные о ключе из базы')
        # query = """ SELECT k.id, k.tag_value, k.state, k.holder_pn, k.status, r.room_code
        #             FROM `keyguard_key` AS k
        #             JOIN `keyguard_room` AS r
        #             ON k.room_id = r.id
        #             WHERE k.tag_value = %(tag_value)s """
        query = """ SELECT id, tag_value, state, holder_pn, status, room_id  
                    FROM `keyguard_key` 
                    WHERE tag_value = %(tag_value)s """
        return self.read_from_db(self.connector, query, {'tag_value': tag_value}, False)
    # id, tag_value, state, holder_pn, number, status, room.id - key
    # id, room_code, name, description, location_id - room

    @log_system.debug_func_call_decorator(logger, True, False, False)
    def return_key(self, id):
        self.logger.info('В базе меняется статус ключа на возвращенный')
        query = """ UPDATE `keyguard_key`
                    SET state = %(tag_issue)s, holder_pn = NULL
                    WHERE id = %(id)s """
        self.write_to_db(self.connector, query, {'tag_issue': 'INPLACE', 'id': id})  # 'tag_issue': 'сдан'

    @log_system.debug_func_call_decorator(logger, True, False, False)
    def take_key(self, id, holder):
        self.logger.info('В базе меняется статус ключа на взятый')
        self.logger.info('Заполняется поле взявшего ключ')
        query = """ UPDATE `keyguard_key`
                    SET state = %(tag_issue)s, 
                    holder_pn = %(tag_holder_pn)s
                    WHERE id = %(id)s """
        self.write_to_db(self.connector, query, {
            'tag_issue': 'ONHANDS', 'tag_holder_pn': holder, 'id': id})  # 'tag_issue': 'выдан'


    # TODO: расширить поиск правила
    @log_system.debug_func_call_decorator(logger, True, False, False)
    def get_rules(self, room_id, user_id, check_status=False):
        self.logger.info('Из базы получаются данные о правах доступа сотрудника к ключу')
        query_dict = {}
        query_dict['query_s_s'] = """
            SELECT id, start_date, end_date, status 
            FROM `keyguard_accessrule`
            WHERE subject_type = %(ss_type)s
            AND subject_id = %(user_id)s
            AND object_type = %(os_type)s
            AND object_id = %(room_id)s"""
        query_dict['query_s_g'] = """
            SELECT r.id, r.start_date, r.end_date, r.status 
            FROM keyguard_accessrule AS r
            JOIN keyguard_rgroup_room AS g
                ON g.rgroup_id = r.object_id
            WHERE g.room_id = %(room_id)s
            AND r.subject_type = %(ss_type)s
            AND r.object_type = %(og_type)s
            AND r.subject_id = %(user_id)s"""
        query_dict['query_g_s'] = """
            SELECT r.id, r.start_date, r.end_date, r.status  
            FROM keyguard_accessrule AS r
            JOIN keyguard_usertogroup AS g
                ON g.group_id = r.subject_id
            WHERE g.user_id = %(user_id)s
            AND r.object_type = %(os_type)s
            AND r.subject_type = %(sg_type)s
            AND r.object_id = %(room_id)s"""
        query_dict['query_g_g'] = """
            SELECT r.id, r.start_date, r.end_date, r.status 
            FROM keyguard_accessrule AS r
            WHERE r.subject_type = %(sg_type)s
            AND r.object_type = %(og_type)s
            AND r.object_id IN (
                SELECT rgroup_id 
                FROM  keyguard_rgroup_room 
                WHERE room_id = %(room_id)s)
            AND r.subject_id IN (
                SELECT group_id 
                FROM keyguard_usertogroup 
                WHERE user_id = %(user_id)s)"""
        if check_status:
            query_dict = dict((k, v+' AND status = 1') for k, v in query_dict.iteritems())
        query = """
            ({query_s_s})
            UNION
            ({query_s_g})
            UNION
            ({query_g_s})
            UNION
            ({query_g_g})""".format(**query_dict)

        return self.read_from_db(self.connector, query, {
            'sg_type': 'GROUP', 'og_type': 'GROUP', 'ss_type': 'SINGLE', 'os_type': 'SINGLE', 'room_id': room_id, 'user_id': user_id}, True)

    def get_rule_schedules_for_day(self, rule, day, check_status=False):
        self.logger.info('Из базы получается расписание для возможного правила')
        query = """ SELECT * 
                    FROM `keyguard_accessschedule`
                    WHERE rule_id = %(rule_id)s
                    AND day_num = %(day_num)s"""
        if check_status:
            query += " AND status = 1"
        return self.read_from_db(self.connector, query, {
            'rule_id': rule, 'day_num': day}, True)

    def get_user_access(self, pn):
        query = """ SELECT
                        v.VALUE 
                    FROM
                        `personal` AS p
                        JOIN `sideparamvalues` AS v
                        JOIN `sideparamtypes` AS t 
                        ON p.ID = v.OBJ_ID 
                        AND v.PARAM_IDX = t.PARAM_IDX 
                    WHERE
                        p.TABID = %(tab_id)s 
                        AND t.NAME = %(parameter_name)s """
        return self.read_from_db(self.sigur, query, {
            'tab_id': pn, 'parameter_name': 'Keyguard - session close permission'}, False)

    @log_system.debug_func_call_decorator(logger, True, False, False)
    def add_tag_record(self, device_id, reader_id, tag_type, tag_value):
        self.logger.info('Записываются данные о прикладываемом объекте в базу')
        query = """ INSERT INTO `keyguard_tagslog`
                    (timestamp, device_id, reader_id, tag_type, tag_value)
                    VALUES (CURRENT_TIMESTAMP, %(device_id)s, 
                    %(reader_id)s, %(tag_type)s, %(tag_value)s) """
        self.write_to_db(self.connector, query, {
            'device_id': device_id, 'reader_id': reader_id,
            'tag_type': tag_type, 'tag_value': tag_value})

    @log_system.debug_func_call_decorator(logger, True, False, False)
    def add_access_record(
            self, device, seance, supn, sspn, key, op_type, op_status):
        self.logger.info('Записываются данные о запросе доступа в базу')
        query = """ INSERT INTO `keyguard_accesslog`
                    (timestamp, device_id, seance_uid, sigur_user_pn, 
                    sigur_security_pn, key_id, 
                    operation_type, operation_status)
                    VALUES (CURRENT_TIMESTAMP, %(device_id)s, 
                    %(seance_id)s, %(sigur_user_pn)s, 
                    %(sigur_security_pn)s, %(key_id)s, 
                    %(operation_type)s, %(operation_status)s) """
        self.write_to_db(self.connector, query, {
            'device_id': device, 'seance_id': seance, 'key_id': key,
            'sigur_user_pn': supn, 'sigur_security_pn': sspn,
            'operation_type': op_type, 'operation_status': op_status})

    @log_system.debug_func_call_decorator(logger, True, True, False)
    def generate_uid(self, device):
        self.logger.info('Генерируется uid сеанса')
        date = datetime.date.today().strftime('%Y%m%d')
        number = '_____'
        uid = '-'.join((device, date, number))
        query = """ SELECT `seance_uid` 
                    FROM (
                        SELECT * 
                        FROM `keyguard_accesslog` 
                        WHERE seance_uid LIKE %(seance_template)s 
                        ORDER BY `timestamp` DESC LIMIT 1
                    ) subx """
        response = self.read_from_db(
            self.connector, query, {'seance_template': uid}, False)
        if response:
            number = str(int(response['seance_uid'].split('-')[2])+1)
        else:
            number = '1'
        return '-'.join((device, date, number.zfill(5)))

    @log_system.debug_func_call_decorator(logger, False, False, True)
    def read_from_db(self, connector, query, parameters, all=True):
        try:
            cursor = connector.cursor(dictionary=True, buffered=True)
            cursor.execute(query, parameters)
            if all:
                response = cursor.fetchall()
            else:
                response = cursor.fetchone()
            cursor.close()
        except mysql.connector.Error as e:
            if self.connector.is_connected():
                raise ConnectionHelperError(
                    'Неудачное чтение данных из базы:\n{}'.format(e))
            raise DBConnectionRefusedError(e)
        else:
            return response

    @log_system.debug_func_call_decorator(logger, False, False, True)
    def write_to_db(self, connector, query, parameters):
        try:
            cursor = connector.cursor(dictionary=True)
            cursor.execute(query, parameters)
            connector.commit()
            cursor.close()
        except mysql.connector.Error as e:
            if self.connector.is_connected():
                raise ConnectionHelperError(
                    'Неудачное внесение/изменение данных в базе:\n{}'.format(e))
            else:
                self.add_request_into_queue(query, parameters)
                raise DBConnectionRefusedError(e)


class ReaderHelper(object):
    logger = log_system.get_logger('app.reader')

    readers_colors = {
        u'default': SL500_RFID_Reader.LED_OFF,
        u'off': SL500_RFID_Reader.LED_OFF,
        u'red': SL500_RFID_Reader.LED_RED,
        u'green': SL500_RFID_Reader.LED_GREEN,
        u'yellow': SL500_RFID_Reader.LED_YELLOW,
    }

    # @property
    # def required_blocks(self):
    #     if not self._required_blocks:
    #         self._required_blocks = [
    #             i for i in range(64) if config.read_config_option(
    #                 'data_blocks', str(i), 'bool') is True]
    #     return self._required_blocks

    def __init__(self):
        try:
            self.inside = None
            self.outside = None
            self.locks = {'inside': threading.Lock(), 'outside': threading.Lock()}
            self.init_rfid()
            self.indications = self.read_indications(
                config.read_config_option('readers', 'indications_path'))
            self._indication = None
            self.side = None
            self._data = None
            self.data_type = None
            self._required_blocks = None

        except Exception as e:
            raise ReaderHelperError(
                'Неудачная попытка инициализации вспомогательной '
                'системы считывателей:\n{}'.format(e))

    @log_system.debug_func_call_decorator(logger, False, False, False)
    def init_rfid(self):
        self.inside = SL500_RFID_Reader(
            config.read_config_option('readers', 'inside_path'), 19200)
        self.outside = SL500_RFID_Reader(
            config.read_config_option('readers', 'outside_path'), 19200)
        key = config.read_config_option('readers', 'key').decode('hex')
        # self.inside.set_key(['\x6A', '\xA9', '\xD7', '\x71', '\x3E', '\xB3'])  # .decode('string-escape'))
        # self.outside.set_key(['\x6A', '\xA9', '\xD7', '\x71', '\x3E', '\xB3'])  # .decode('string-escape'))
        self.outside.set_key(key)
        self.inside.set_key(key)
        self.change_light('outside', 'off')
        self.change_light('inside', 'off')

    @log_system.debug_func_call_decorator(logger, False, False, False)
    def reconnect(self):
        try:
            self.init_rfid()
        except:
            self.logger.debug('Неудачная попытка соединения со считывателями')
            sleep(10)
            self.reconnect()
        else:
            self.logger.info('Соединение со считывателями восстановлено')
            self.reset_temp_data()

    @log_system.debug_func_call_decorator(logger, True, False, True)
    def read_indications(self, path):
        with open(path, 'r') as f:
            return json.loads(f.read())

    @log_system.debug_func_call_decorator(logger, True, False, False)
    def set_indication(self, name):
        try:
            self._indication = self.indications[name]
        except KeyError:
            raise ReaderHelperError(
                'Не найдено параметров индикации '
                'с заданным названием {}'.format(name))

    # если data есть массив, то вызывается ошибка
    @property
    def data(self):
        if type(self._data) != str and self._data is not None:
            raise ReaderHelperError(
                "Невозможно передать аттрибут data, "
                "так как сначала нужно распознать тип")
        else:
            return self._data

    @data.setter
    def data(self, data):
        self._data = data

    @log_system.debug_func_call_decorator(logger, False, False, True)
    def wait_data(self):
        while True:
            data = self.supervise()
            if data:
                self.data = data
                break
            sleep(0.01)

    def supervise(self, inside=True, outside=True):
        if inside:
            # print('read inside')
            key_data = self.read(self.inside)
            if key_data:
                self.side = 'inside'
                return key_data
        if outside:
            # print('read outside')
            key_data = self.read(self.outside)
            if key_data:
                self.side = 'outside'
                return key_data

    # @log_system.debug_func_call_decorator(logger, False, True, True)
    def read(self, reader):
        try:
            required_block = config.read_config_option('system', 'block', 'int')
            if not required_block:
                self.logger.warn('В файле конфигурации не выбран блок для считывания')
                raise ReaderHelperError(
                    'Не выбрано блока для считывания')
            data = reader.read_block(required_block)
            if not data:
                sleep(0.05)
                return None
        except (ReaderHelperError, TimeoutRaisedError):
            raise
        except KeyboardInterrupt:  # delete
            raise
        # except IndexError as e:
        #     self.logger.error('Неверный индекс: {}'.format(e))
        except Exception as e:
            self.logger.error('Возникло исключение {}: {}'.format(type(e).__name__, e))
            raise ReaderIOError(e)
        else:
            return data

    def get_active_side_names(self, inside=True, outside=True):
        return [k for k, v in {'inside': inside, 'outside': outside}.items() if v is True]

    @log_system.debug_func_call_decorator(logger, False, False, False)
    def try_to_recognize(self):
        if self.data.startswith(config.read_config_option('system', 'key_beginning')):
            self.data_type = 'key'
            self.logger.debug('Распознан тип данных ключ')


    # def find_current_data_index(self):
    #     index = None
    #     for key, value in self._data.items():
    #         if value[0] != '0':
    #             if not index:
    #                 print(key)
    #                 index = key
    #             else:
    #                 raise ReaderHelperError(
    #                     'Данные присутствуют в нескольких блоках карты')
    #     if index == None:
    #         raise ReaderHelperError(
    #             'В заданных для считывания блоках нет данных для распознавания')
    #     return index

    @log_system.debug_func_call_decorator(logger, False, False, False)
    def reset_temp_data(self):
        self._data = None
        self.data_type = None
        self.side = None

    def indicate(self):
        ind_l = []
        ind_s = []
        for side_k, side_v in self._indication.items():
            if side_v['sound']['status']:
                sound = threading.Thread(target=self.make_sound, args=(
                    # self.get_reader_by_side(side_k),
                    side_k,
                    int(side_v['sound']['duration']),
                    int(side_v['sound']['numbers'])))
                sound.daemon = True
                ind_s.append(sound)
            if side_v['light']['status']:
                light = threading.Thread(target=self.change_light, args=(
                    # self.get_reader_by_side(side_k),
                    side_k,
                    side_v['light']['color'],
                    side_v['light']['blink']))
                light.daemon = True
                ind_l.append(light)
        for s in ind_s:
            s.start()
        for l in ind_l:
            l.start()

        for s in ind_s:
            s.join()
        for l in ind_l:
            l.join()

    @log_system.debug_func_call_decorator(logger, True, False, False)
    def change_light(self, side, color, blink=False):
        try:
            reader = self.get_reader_by_side(side)

            self.locks[side].acquire()

            previous = reader.current_light
            reader.current_light = self.readers_colors[color]
            if blink and previous:
                self.logger.debug('Обработка мигания на стороне {} цвета {}'.format('', color))
                if reader.current_light == previous:
                    self.logger.debug('Новый цвет совпадает с текущим')
                    reader.current_light = self.readers_colors['default']
                    self.locks[side].release()
                    sleep(0.1)
                    self.locks[side].acquire()
                    reader.current_light = previous
                else:
                    self.locks[side].release()
                    sleep(0.3)
                    self.locks[side].acquire()
                    reader.current_light = previous
        except ValueError as e:
            raise ReaderHelperError(
                "Неверное значение параметра {} "
                "при изменении цвета диода".format(e))
        except Exception as e:
            raise ReaderHelperError(
                'Ошибка изменения цвета:\n{}'.format(e))
        finally:
            self.locks[side].release()

    @log_system.debug_func_call_decorator(logger, True, False, False)
    def make_sound(self, side, duration, number=1):
        try:
            reader = self.get_reader_by_side(side)
            waiting = self._calculate_waiting(duration)
            for i in range(number):
                self.locks[side].acquire()
                self.sound_signal(reader, duration)
                if number != 1 and i != number-1:
                    self.locks[side].release()
                    sleep(waiting)
        except (ValueError, TypeError) as e:
            raise ReaderHelperError(
                "Неверное значение параметра {} "
                "при осуществлении звуковой индикации".format(e))
        except Exception as e:
            raise ReaderHelperError(
                'Ошибка вызова звуковой индикации:\n{}'.format(e))
        finally:
            self.locks[side].release()

    def sound_signal(self, reader, duration):
        for i in range(4):
            response = reader.rf_beep(duration)
            if not reader.check_wrong_response(response):
                self.logger.debug('Сигнал должен прозвучать')
                break
        else:
            self.logger.debug(
                'Все попытки на осуществление звуковой индикации закончились')
            raise ReaderHelperError('Не удалось издать сигнал на считывателе')

    @log_system.debug_func_call_decorator(logger, True, True, True)
    def _calculate_waiting(self, duration):
        waiting = duration / 50
        if waiting < 0.2:
            waiting = 0.2
        return waiting

    @log_system.debug_func_call_decorator(logger, True, True, False)
    def get_reader_by_side(self, side):
        if side == u'inside':
            return self.inside
        elif side == u'outside':
            return self.outside
        else:
            raise ValueError('side')
