#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod
import config_p2 as config
import log_system
import datetime
import requests

module_logger = log_system.get_logger('app.operation')


class OperationFactory(object):
    @staticmethod
    @log_system.debug_func_call_decorator(module_logger, True, True, False)
    def create_operation(type, side, session_existence, timeout):
        if not timeout:
            if type == 'card' and side == 'inside' and session_existence:
                module_logger.info('Создана операция приложенной внутри карты во время сеанса')
                return SCardInsideOperation()
            elif type == 'card' and side == 'outside' and not session_existence:
                module_logger.info('Создана операция приложенной снаружи карты до сеанса')
                return CardOutsideOperation()
            elif type == 'key' and side == 'inside' and session_existence:
                module_logger.info('Создана операция приложенного внутри ключа во время сеанса')
                return SKeyInsideOperation()
            # elif type == 'card' and side == 'inside' and not session_existence:
                # return CardInsideOperation()
            # elif type == 'card' and side == 'outside' and session_existence:
                # return SCardOutsideOperation()
            # elif type == 'key' and side == 'inside' and not session_existence:
                # return KeyInsideOperation()
            # elif type == 'key' and side == 'outside' and session_existence:
                # return SKeyOutsideOperation()
            # elif type == 'key' and side == 'outside' and not session_existence:
                # return KeyOutsideOperation()
            else:
                module_logger.info('Создана операция по обработке остальных случаев')
                return RestOperation()
        else:
            module_logger.info('Создана операция окончания сеанса')
            return EndingOperation()


class Operation(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        self._helper = None

    @property
    def helper(self):
        if self._helper:
            return self._helper
        else:
            raise ValueError('Не установлен helper')

    @helper.setter
    def helper(self, helper):
        self._helper = helper

    @log_system.debug_func_call_decorator(module_logger, False, False, True)
    def tag_log(self):
        reader_id = None
        if self.helper.reader.side == 'inside':
            reader_id = config.read_config_option(
                'readers', 'inside_reader_id', 'str')
        elif self.helper.reader.side == 'outside':
            reader_id = config.read_config_option(
                'readers', 'outside_reader_id', 'str')
        device_id = config.read_config_option(
            'system', 'device_id', 'int')
        tag_type = self.helper.reader.data_type
        tag_value = self.helper.reader.data
        self.helper.connection.add_tag_record(
            device_id, reader_id, tag_type, tag_value)

    def form_codekey(self, card):
        code = '38' + card[:14]
        return bytearray(code.decode('hex'))


    def close(self):
        self.helper.reader.reset_temp_data()

    @abstractmethod
    @log_system.debug_func_call_decorator(module_logger, False, False, False)
    def execute(self):
        pass


class SCardInsideOperation(Operation):
    def execute(self):
        reader = self.helper.reader
        # info = self.helper.connection.get_user_info(reader.data)
        code = self.form_codekey(reader.data)
        try:
            pn = self.helper.connection.get_user_pn(code)['TABID']
            res = self.helper.connection.get_user_access(pn)
            access = res['VALUE']
        except TypeError:
            module_logger.debug(
                'Информации о пользователе c меткой {} '
                'не найдено'.format(reader.data))
            reader.set_indication('failure')
        except KeyError as e:
            module_logger.warn(
                'В данных, полученных из базы, отсутствует значение '
                'по указанному ключу: {}'.format(e))
            module_logger.debug(
                'В результате о пользователе с меткой {} отсутствует '
                'значение по указанному ключу: {}'.format(reader.data, e))
            reader.set_indication('failure')
        else:
            if int(access) == 1:
                self.helper.session.sspn = pn
                self.helper.connection.add_security_number_for_seance(
                    self.helper.session.uid, pn)
                self.helper.session.end()
                reader.set_indication('ending')
            else:
                reader.set_indication('failure')


class CardOutsideOperation(Operation):


    def execute(self):
        data = self.helper.reader.data
        # info = self.helper.connection.get_user_info(data)
        code = self.form_codekey(data)

        try:
            # pn = info['personnel_number']
            pn = self.helper.connection.get_user_pn(code)['TABID']
            self.helper.session.uid = self.helper.connection.generate_uid(
                config.read_config_option('system', 'device_id'))
            self.helper.session.supn = pn
            self.helper.session.start_session()
            self.helper.reader.set_indication('valid_card')
        except TypeError as e:
            module_logger.debug(
                'Информации о пользователе c меткой {} '
                'не найдено'.format(data))
            self.helper.reader.set_indication('failure')
        except KeyError as e:
            module_logger.warn(
                'В информации из базы данных отсутствует значение '
                'по указанному ключу: {}'.format(e))
            module_logger.debug(
                'В результате о пользователе с меткой {} отсутствует '
                'значение по указанному ключу: {}'.format(data, e))
            self.helper.reader.set_indication('failure')


class SKeyInsideOperation(Operation):

    def __init__(self):
        super(SKeyInsideOperation, self).__init__()
        self.key_id = None
        self.type = None
        self.status = None

    @log_system.debug_func_call_decorator(module_logger, False, False, False)
    def access_log(self):
        device_id = config.read_config_option('system', 'device_id')
        session = self.helper.session
        seance_id = session.uid
        user_number = session.supn
        security_number = session.sspn
        key_id = self.key_id
        # operation_type = self.type
        if self.type == 'сдача':
            operation_type = 'RETURN'
        elif self.type == 'выдача':
            operation_type = 'TAKE'
        else:
            operation_type = None

        if self.status:
            operation_status = 'SUCCESS'
        else:
            operation_status = 'FAILURE'

        connection = self.helper.connection
        connection.add_access_record(
            device_id, seance_id, user_number, security_number,
            key_id, operation_type, operation_status)

    def execute(self):
        data = self.helper.reader.data
        info = self.helper.connection.get_key_info(data)
        try:
            self.key_id = info['id']
            self.type = self.determine_operation_type(info)
            if self.type == 'сдача':
                self.return_key(info)
            elif self.type == 'выдача':
                self.take_key(info)
            else:
                self.helper.reader.set_indication('failure')
            self.access_log()
        except TypeError as e:
            module_logger.debug(
                'Информации о ключе c меткой {} '
                'не найдено: {}'.format(data, e))
            self.helper.reader.set_indication('failure')
        except KeyError as e:
            module_logger.warn(
                'В данных, полученных из базы, отсутствует значение '
                'по указанному ключу: {}'.format(e))
            module_logger.debug(
                'В результате о ключе с меткой {} отсутствует '
                'значение по указанному ключу: {}'.format(data, e))
            self.helper.reader.set_indication('failure')

    @log_system.debug_func_call_decorator(module_logger, False, True, False)
    def determine_operation_type(self, key):
        if key['state'] == u'ONHANDS':  # u'выдан':
            return 'сдача'
        elif key['state'] == u'INPLACE':  # u'сдан':
            return 'выдача'
        else:
            module_logger.debug(
                'Неверное значение состояния ({}) для '
                'ключа с id = {}'.format(key['state'], key['id']))
            module_logger.warn('Неверный тип операции в базе')
            return None

    @log_system.debug_func_call_decorator(module_logger, False, False, False)
    def return_key(self, key):
        self.helper.connection.return_key(key['id'])
        self.status = True
        self.helper.reader.set_indication('successful_returning')

    @log_system.debug_func_call_decorator(module_logger, False, False, False)
    def take_key(self, key):
        if self.check_user_access_to_key(key['room_id']) and \
                self.check_key_status(key['status']):
            self.helper.connection.take_key(
                key['id'], self.helper.session.supn)
            self.status = True
            self.helper.reader.set_indication('successful_taking')
        else:
            self.status = False
            self.helper.reader.set_indication('failure')
        # self.helper.connection.take_key(
        #     key['id'], self.helper.session.supn)
        # self.status = True
        # self.helper.reader.set_indication('successful_taking')


    @log_system.debug_func_call_decorator(module_logger, False, True, False)
    def check_user_access_to_key(self, room_id):
        try:
            url = config.read_config_option(
                'requests', 'check_access', 'str')
            params = {'personal': self.helper.session.supn, 'room': room_id}
            res = requests.get(url, params=params).json()
            if res == 'GRANTED':
                return True
            elif res == 'DENIED':
                return False
            # rules = self.form_access_rules(room_id, self.helper.session.supn)
            # rules = self.helper.connection.get_rules(
            #     room_id, self.helper.session.supn, check_status=True)
            # for rule in rules:
            #     module_logger.info(rule['id'])
            #     # if rule['status'] == 1:
            #     if self.check_in_interval('date', rule['start_date'], rule['end_date']):
            #         schedule = self.get_today_schedule(rule['id'])
            #         if schedule:
            #             if self.check_in_interval('time', schedule['start_time'], schedule['end_time']):
            #                 module_logger.info('Доступ разрешен')
            #                 module_logger.debug('Найдено подходящее правило для ...')  # дописать
            #                 return True
        except TypeError as e:
            module_logger.debug(
                'Неправильные типы данных: {}'.format(e))  # изменить формулировку
        except Exception as e:
            module_logger.debug('{}'.format(e))
        return False

    # def form_access_rules(self, room_id, user_id):
    #     return []

    def get_today_schedule(self, rule_id):
        weekday = str(datetime.datetime.now().weekday())
        schedule = self.helper.connection.get_rule_schedules_for_day(rule_id, weekday, check_status=True)
        if len(schedule) > 1:
            module_logger.warning(
                'В базе больше одного рабочего расписания '
                'на день {} для правила {}'.format(weekday, rule_id)
            )
            # schedule = schedule[0]
        if schedule:
            return schedule[0]


    @log_system.debug_func_call_decorator(module_logger, False, True, False)
    def check_key_status(self, key_status):
        return key_status == u'AVAILABLE'

    @log_system.debug_func_call_decorator(module_logger, True, True, False)
    def check_in_interval(self, type, start, end):
        now = datetime.datetime.now()
        if type == 'date':
            current = now.date()
        elif type == 'time':
            current = now.time()
            start = (start + datetime.datetime.min).time()
            end = (end + datetime.datetime.min).time()
        else:
            module_logger.warning('Неправильный тип при проверки вхождения в интервал')
            return False
        return current >= start and current <= end


# class KeyInsideOperation(Operation):
#     pass

# class SKeyOutsideOperation(Operation):
    # pass

# class KeyOutsideOperation(Operation):
#     pass

# class CardInsideOperation(Operation):
#     pass

# class SCardOutsideOperation(Operation):
    # pass


class RestOperation(Operation):
    def execute(self):
        self.helper.reader.set_indication('failure')


class EndingOperation(Operation):
    def execute(self):
        self.helper.session.end()
        self.helper.reader.set_indication('ending')

    def tag_log(self):
        pass


